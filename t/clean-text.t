#! /usr/bin/env perl6

use v6.c;

use IRC::Client::Plugin::UrlTitle;
use Test;

plan 4;

my IRC::Client::Plugin::UrlTitle $plugin .= new;

is $plugin.clean-text("\r\n"), "", 'Lone \r\n sequence is removed';
is $plugin.clean-text("foo\r\nbar"), "foo bar", '\r\n sequence in string gets replaced to space';
is $plugin.clean-text("foo     bar"), "foo bar", 'multiple spaces get condensed to a single space';
is $plugin.clean-text("foo     "), "foo", 'Trailing whitespace is trimmed away';

# vim: ft=perl6 noet
